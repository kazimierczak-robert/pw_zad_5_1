﻿// Zad 5.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include "windows.h"
#include "time.h"

#define random() (float)rand()/(float)RAND_MAX
#define nmax 10

typedef int t2[nmax][nmax];
typedef int t1[nmax];
CRITICAL_SECTION SekcjaKrytyczna;
t2 A;
volatile int szerokosc;

int silnia(int n)
{
	{
		if (n == 0 || n == 1) return 1;
		else return n*silnia(n - 1);
	}
}
void gnp(int n, float p, t2 A) 
{
	int i, j;
	for (i = 0; i < n; i++) A[i][i] = 0;
	{
		for (i = 0; i < n - 1; i++)
		{
			for (j = i + 1; j < n; j++)
			{
				A[i][j] = A[j][i] = (random() <= p);
			}
		}
	}
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			printf("%d ", A[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
void permInit(int n, t1 p, int threadID) 
{ 
	int i; 
	if (threadID == 0)
	{
		for (i = 0; i < n; i++)
		{
			p[i] = i;
		}
	}
	else {
		p[0] = threadID;
		for (i = 1; i < n; i++)
		{
			p[i] = i - 1;
			if (i == threadID) break;
		}
		p[i] = i - 1;
		i++;
		for (i = i; i < n; i++)
		{
			p[i] = i;
		}
	}
}
void druk(int n, int L, t1 p) 
{
	int i;
	printf("%d: ", L);
	for (i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
		printf("\n");
}
int permNext(int n, t1 p) 
{
	int i, j, x, b;
	b = 0;
	i = n-1;
	while (i>0) 
	{
		if (p[i]>p[i-1])
		{
			j = n-1;
			while (p[j] < p[i-1])
			j--;
			x = p[j]; 
			p[j] = p[i-1]; 
			p[i-1] =x;
			while (i<n) 
			{
				x = p[i]; 
				p[i] = p[n-1]; 
				p[n-1] =x;
				i++; 
				n--;
			};
			b = 1;
			break;
		}
	i--;
	}
	return b;
}
int naj_roznica(int n, t2 A, t1 perm) 
{
	int dist, best = 1;
		int i, j;
		for (i = 0; i < n - 1; i++)
		{
			for (j = i + 1; j < n; j++)
			{
				if (A[i][j])
				{
					dist = abs(perm[i] - perm[j]);
					if (dist > best) 
					{ 
						best = dist; 
					}
				}
			}
		}
	return best;
}
DWORD WINAPI ThreadFunc_SzerokoscGrafu(LPVOID lpdwParam)
{
	t1 perm;
	int n = nmax;
	int L = 1;
	int max_il_perm_na_watek = silnia(nmax - 1);
	permInit(nmax, perm, (int)lpdwParam);
	int naj_r;
	do
	{
		naj_r = naj_roznica(n, A, perm);
		EnterCriticalSection(&SekcjaKrytyczna);
		if (naj_r <= szerokosc) szerokosc = naj_r;
		if (1 == szerokosc || L == max_il_perm_na_watek) break;
		LeaveCriticalSection(&SekcjaKrytyczna);
		//printf("%d NR %d\n", (L + max_il_perm_na_watek*(int)lpdwParam),naj_r);
		//druk(n, (L + max_il_perm_na_watek*(int)lpdwParam), perm);
		L++;
	} while (permNext(n, perm));
	if (1 == szerokosc || L == max_il_perm_na_watek) 
	{
		LeaveCriticalSection(&SekcjaKrytyczna);
		//printf("%d NR %d\n", (L + max_il_perm_na_watek*(int)lpdwParam), naj_r);
	}
	return 0;
}
int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	float p = 0.5f;
	gnp(nmax, p, A);
	szerokosc = nmax-1;
	DWORD dwThreadId;
	HANDLE ahThread[nmax];
	InitializeCriticalSection(&SekcjaKrytyczna);
	t1 indeksy;
	for (int i = 0; i < nmax; i++)
	{
		indeksy[i] = i;
	}
	for (int i = 0; i < nmax; i++)
	{
		ahThread[i] = CreateThread(NULL, 0, ThreadFunc_SzerokoscGrafu, (PVOID)indeksy[i], 0, &dwThreadId);
	}
	for (int i = 0; i < nmax; i++)
	{
		WaitForSingleObject(ahThread[i], INFINITE);
	}
	printf("\nSzerokosc grafu %d\n", szerokosc);
	for (int i = 0; i < nmax; i++)
	{
		CloseHandle(ahThread[i]);
	}
	DeleteCriticalSection(&SekcjaKrytyczna);
	system("pause");
	return 0;
}